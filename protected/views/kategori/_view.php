<?php
/* @var $this KategoriController */
/* @var $data Kategori */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_kategory')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_kategory), array('view', 'id'=>$data->id_kategory)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_kategory')); ?>:</b>
	<?php echo CHtml::encode($data->nama_kategory); ?>
	<br />


</div>