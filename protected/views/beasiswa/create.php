<?php
/* @var $this BeasiswaController */
/* @var $model Beasiswa */

$this->breadcrumbs=array(
	'Beasiswas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Beasiswa', 'url'=>array('index')),
	array('label'=>'Manage Beasiswa', 'url'=>array('admin')),
);
?>

<h1>Create Beasiswa</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>