<?php
/* @var $this BeasiswaController */
/* @var $model Beasiswa */

$this->breadcrumbs=array(
	'Beasiswas'=>array('index'),
	$model->id_beasiswa,
);

$this->menu=array(
	array('label'=>'List Beasiswa', 'url'=>array('index')),
	array('label'=>'Create Beasiswa', 'url'=>array('create')),
	array('label'=>'Update Beasiswa', 'url'=>array('update', 'id'=>$model->id_beasiswa)),
	array('label'=>'Delete Beasiswa', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_beasiswa),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Beasiswa', 'url'=>array('admin')),
);
?>

<h1>View Beasiswa #<?php echo $model->id_beasiswa; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_beasiswa',
		'nama_beasiswa',
		'id_kategory',
	),
)); ?>
