<?php
/* @var $this BeasiswaController */
/* @var $model Beasiswa */

$this->breadcrumbs=array(
	'Beasiswas'=>array('index'),
	$model->id_beasiswa=>array('view','id'=>$model->id_beasiswa),
	'Update',
);

$this->menu=array(
	array('label'=>'List Beasiswa', 'url'=>array('index')),
	array('label'=>'Create Beasiswa', 'url'=>array('create')),
	array('label'=>'View Beasiswa', 'url'=>array('view', 'id'=>$model->id_beasiswa)),
	array('label'=>'Manage Beasiswa', 'url'=>array('admin')),
);
?>

<h1>Update Beasiswa <?php echo $model->id_beasiswa; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>