<?php
/* @var $this BeasiswaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Beasiswas',
);

$this->menu=array(
	array('label'=>'Create Beasiswa', 'url'=>array('create')),
	array('label'=>'Manage Beasiswa', 'url'=>array('admin')),
);
?>

<h1>Beasiswas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
