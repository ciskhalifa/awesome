<?php
/* @var $this BeasiswaController */
/* @var $data Beasiswa */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_beasiswa')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_beasiswa), array('view', 'id'=>$data->id_beasiswa)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_beasiswa')); ?>:</b>
	<?php echo CHtml::encode($data->nama_beasiswa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_kategory')); ?>:</b>
	<?php echo CHtml::encode($data->id_kategory); ?>
	<br />


</div>